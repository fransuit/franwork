
$(document).ready(function(){  //esperar a que se carge la pagina para ejecutar javascript



    function actualitzarMatriu(m1,m2) {
        for(var i=0; i<3; i++) {
            for (var j = 0; j < 3; j++) {
                if(m1[i][j]!=m2[i][j]){
                    var id=(3*i)+j;
                    // alert(id)
                    if(m1[i][j]===1){
                        //$("#tablero button #1").css("background-color","green");
                        document.getElementById(id).style.backgroundColor="green";
                        element2.innerHTML="TURNO PARA LOS ROJOS";
                        $("#text").css("color","red");
                    }else{
                        //$("#tablero button #"+id).css("background-color","red");
                        document.getElementById(id).style.backgroundColor="red";
                        element2.innerHTML="TURNO PARA LOS VERDES";
                        $("#text").css("color","green");

                    }
                }
            }
        }
    }

    function guanyat(matrix,p) {
        b3 = true;
        b4 = true;
        var acaba=false;
        casellesBuides = 0;
        for (var i = 0; i < 3; i++) {
            b1 = true;
            b2 = true;
            var v = 0;
            var w = 3;
            for (var j = 0; j < 3; j++) {                  //estos bucles recorren el tablero y compruevan si hay 3 casellas alineadas del mismo color
                if (matrix[i][j] === 0) casellesBuides++;
                w--;
                b1 = b1 && matrix[i][j] === p;
                b2 = b2 && matrix[j][i] === p;    //ERROR
                if (i == 0) {
                    b3 = b3 && matrix[v][j] === p;
                    b4 = b4 && matrix[w][j] === p;
                }
                v++;

            }
            if (b1 === true || b2 === true || b3 === true || b4 === true) {
                acaba = true
            }   //si se encuantra 3 seguidas se acaba la partida
        }
        return acaba;
    }

    //variables de la partida
    var player=0;
    var torn=false;
    var acabat=false;
    var matrix=[];
    for(var i=0; i<3; i++) {
        matrix[i] = new Array(3);
    }
    for(var i=0; i<3; i++){
        for(var j=0; j<3; j++){   //inicializamos todos los componentes de la matriz a 0
            matrix[i][j]=0;

        }

    }

    var casellesBuides=0;
    $("#restart").click(function(){    //cuando se clique al boton restart va a recargar la pagina

        location.reload(true);
    });


    $.get("/login", function(res){
        //$("#txt").html("player"+res);
        player=parseInt(res);
        if(player===1){
            torn=true;

        }
    });

    var element2 = document.getElementById("text");
    element2.innerHTML="TURNO PARA LOS VERDES";
    $("#text").css("color","green");

    function clock() {
        //alert(player);
        if(player!==0) {
            $.ajax(
                {
                    url : '/contador',
                    type: "POST",
                    //data : {savis: player },
                })
                .done(function(data) {
                    var matrix2=JSON.parse(data);
                    for(var i=0; i<3; i++){
                        for(var j=0; j<3; j++){
                            if(matrix2[i][j]!==matrix[i][j]){
                                actualitzarMatriu(matrix2,matrix);
                                matrix=matrix2;
                                torn=true;
                                var p=0;
                                if(player==1){p=2}else{p=1}
                                if(guanyat(matrix,p)){
                                    var color;
                                    var color2;
                                    if (player == 2) {
                                        color = "green";
                                        color2 = "VERDES";
                                    } else {
                                        color = "red";
                                        color2 = "ROJOS";
                                    }
                                    acabat = true;
                                    element2.innerHTML = "";
                                    $("#fons").css("background-color", color);
                                    var element3 = document.getElementById("victoria");
                                    element3.innerHTML = "VICTORIA PARA LOS " + color2 + "!";
                                    $("#fons").animate({height: "30%"}, 1200);
                                }
                                //alert("el rival ha clicat");
                            }
                        }
                    }


                });
        }

    }

    setInterval(clock,500);


    $("#tablero button").hover( //definimos comportamientos para que cuando se situe el cursor encima de una casilla cambie el color de fondo
        function() {
            var ie=$(this).attr("id");
            var a=parseInt((ie/3),10);  //calculo para obtener coordenadas x e y teniendo como parametro un id
            var b=ie%3;

            if(matrix[a][b]==0){
                if(player==1){
                    $(this).css("background-color","green"); //segun si es el turno del jugador verde o rojo, la casilla se podra de un color u otro
                }else{
                    $(this).css("background-color","red");
                }
            }

        }, function() {

            var ie=$(this).attr("id");
            var a=parseInt((ie/3),10); //cuando el cursor ya no este encima, la casella volvera a su color original,siempre y cuando no se haga click
            var b=ie%3;
            if(matrix[a][b]==0){
                $(this).css("background-color","#DEDEDE");
            }
        }
    );


    $("#tablero button").click(function(){  //expresion para detectar cuando se hace click encima de una casilla


        var e=$(this).attr("id");
        var a=parseInt((e/3),10);      //se lee el ID de la casilla y se hacen calculos para obtener el eje x e y de el tablero que corresponde a la ID
        var b=e%3;
        //alert(torn)
        if(matrix[a][b]===0 && torn===true && acabat===false) {      //si la casilla clicada esta vacia y la patida aun no ha terminado:

            matrix[a][b] = player;
            torn = false;
            $("button").click(function () {
                $.ajax(
                    {
                        url: '/',
                        type: "POST",
                        data: {bonsai: JSON.stringify(matrix)}
                    })
                    .done(function (data) {
                        //$("#contador").text(data);
                        //alert("swssssss");
                    })

            });


            if (player == 1) {
                $(this).css("background-color", "green");

                element2.innerHTML="TURNO PARA LOS ROJOS";
                $("#text").css("color","red");

            }
            if (player == 2) {
                $(this).css("background-color", "red");
                element2.innerHTML="TURNO PARA LOS VERDES";
                $("#text").css("color","green");
            }    //cambiamos el color de fondo de la casill, segun el turno actual


            element2 = document.getElementById("text");

            acabat=guanyat(matrix,player);

            if (acabat) {         //si la partida se ha acabado:
                var color;
                var color2;
                if (player == 1) {
                    color = "green";
                    color2 = "VERDES";
                } else {
                    color = "red";
                    color2 = "ROJOS";
                }
                acabat = true;
                element2.innerHTML = "";
                $("#fons").css("background-color", color);
                var element3 = document.getElementById("victoria");
                element3.innerHTML = "VICTORIA PARA LOS " + color2 + "!";
                $("#fons").animate({height: "30%"}, 1200);       //sale una animacion anunciando el ganador y con un boton restart
            } else {     //si la partida no se ha acabado:

                /*
                    if(torn==true){
                        torn=false;
                        element2.innerHTML="TURNO PARA LOS ROJOS";
                         $("#text").css("color","red");
                      }else{                                           //se canvia de turno
                        torn=;
                       element2.innerHTML="TURNO PARA LOS VERDES";
                          $("#text").css("color","green");
                      }
                  }
                  */

                if (acabat == false && casellesBuides == 0) {    //si no hay ningun ganador y todas las casillas estan ocupadas: EMPATE
                    element2.innerHTML = "";
                    $("#fons").css("background-color", "#B3B3B3");
                    element3 = document.getElementById("victoria");
                    element3.innerHTML = "LA PARTIDA ACABA EN EMPATE!"
                    $("#fons").animate({height: "30%"}, 1300);    //sale una animacion anunciando que hay empate y con un boton restart
                }
            }
        }
    });
});
